
# Build for riscv64

## Prepare

To build elf2flat standalonely, update `filenames.h`, `elf/riscv.h` and
`elf/reloc-macros.h` at first:

```sh
$ wget -c https://github.com/gcc-mirror/gcc/raw/master/include/filenames.h

$ mkdir elf/ && cd elf/
$ wget -c https://raw.githubusercontent.com/bminor/binutils-gdb/master/include/elf/riscv.h
$ wget -c https://raw.githubusercontent.com/bminor/binutils-gdb/master/include/elf/reloc-macros.h
```

Please make sure the versions used are right.

And then, we install such packages to prepare the other headers:

```sh
$ sudo apt install binutils-dev libiberty-dev
```

And then the cross compiler:

```sh
$ sudo apt install gcc-riscv64-linux-gnu
```

## Configuration


```sh
$ ./configure --prefix=/usr --target=riscv64-linux-gnu -with-bfd-include-dir=/usr/include/ \
--with-binutils-include-dir=/usr/include/ \
--with-libbfd=/usr/lib/riscv64-linux-gnu/libbfd.a \
--with-libiberty=/usr/lib/riscv64-linux-gnu/libiberty.a \
--disable-werror --build=riscv64-linux-gnu
```

To compile with the headers from buildroot (requires to modify the path of libiberty in elf/riscv.h):

```sh
$ ./configure --prefix=/usr --target=riscv64-linux-gnu -with-bfd-include-dir=/labs/linux-lab/build/riscv64/virt/buildroot/2022.11.1/build/host-binutils-2.38/bfd/ --with-binutils-include-dir=/labs/linux-lab/build/riscv64/virt/buildroot/2022.11.1/build/host-binutils-2.38/include/ --with-libbfd=/labs/linux-lab/build/riscv64/virt/buildroot/2022.11.1/build/host-binutils-2.38/bfd/libbfd.a --with-libiberty=/labs/linux-lab/build/riscv64/virt/buildroot/2022.11.1/build/host-binutils-2.38/libiberty/libiberty.a --disable-werror --disable-ld-elf2flt-binary
```

## Building

```sh
$ make
$ sudo make install
```

## Usage

Please refer to [Linux Lab](https://gitee.com/tinylab/linux-lab)
